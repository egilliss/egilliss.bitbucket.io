var annotated_dup =
[
    [ "Elevator", null, [
      [ "Button", "classElevator_1_1Button.html", "classElevator_1_1Button" ],
      [ "Elevator", "classElevator_1_1Elevator.html", "classElevator_1_1Elevator" ],
      [ "Motor", "classElevator_1_1Motor.html", "classElevator_1_1Motor" ]
    ] ],
    [ "Encoder", null, [
      [ "encoder", "classEncoder_1_1encoder.html", "classEncoder_1_1encoder" ]
    ] ],
    [ "Firmware", null, [
      [ "ClosedLoop", "classFirmware_1_1ClosedLoop.html", "classFirmware_1_1ClosedLoop" ]
    ] ],
    [ "Lab0x02b", null, [
      [ "Light", "classLab0x02b_1_1Light.html", "classLab0x02b_1_1Light" ],
      [ "LightController", "classLab0x02b_1_1LightController.html", "classLab0x02b_1_1LightController" ],
      [ "NUCLEO", "classLab0x02b_1_1NUCLEO.html", "classLab0x02b_1_1NUCLEO" ],
      [ "vLED", "classLab0x02b_1_1vLED.html", "classLab0x02b_1_1vLED" ]
    ] ],
    [ "Lab0x04", null, [
      [ "CollectionTask", "classLab0x04_1_1CollectionTask.html", "classLab0x04_1_1CollectionTask" ]
    ] ],
    [ "Lab0x05", null, [
      [ "UIBlinker", "classLab0x05_1_1UIBlinker.html", "classLab0x05_1_1UIBlinker" ]
    ] ],
    [ "Lab0x06", null, [
      [ "MotorDriver", "classLab0x06_1_1MotorDriver.html", "classLab0x06_1_1MotorDriver" ]
    ] ],
    [ "Lab0x07", null, [
      [ "ClosedLoop", "classLab0x07_1_1ClosedLoop.html", "classLab0x07_1_1ClosedLoop" ]
    ] ],
    [ "Lab0x07Physical", null, [
      [ "Lab0x07Op", "classLab0x07Physical_1_1Lab0x07Op.html", "classLab0x07Physical_1_1Lab0x07Op" ]
    ] ],
    [ "Physical", null, [
      [ "Phys", "classPhysical_1_1Phys.html", "classPhysical_1_1Phys" ]
    ] ],
    [ "UART0x05", null, [
      [ "UART0x05", "classUART0x05_1_1UART0x05.html", "classUART0x05_1_1UART0x05" ]
    ] ]
];