var classLab0x04_1_1CollectionTask =
[
    [ "__init__", "d1/db0/classLab0x04_1_1CollectionTask.html#a6900717f6b8f9d42006264a9e069625f", null ],
    [ "run", "d1/db0/classLab0x04_1_1CollectionTask.html#a008bd99b783065d027c07fff9ba5cb00", null ],
    [ "curr", "d1/db0/classLab0x04_1_1CollectionTask.html#a154fb33e214d05674c94e66e5d67fb44", null ],
    [ "Encoder1", "d1/db0/classLab0x04_1_1CollectionTask.html#aba15a53cd5abe5b6f3267332942a8f12", null ],
    [ "Encoder2", "d1/db0/classLab0x04_1_1CollectionTask.html#afc72d4ca25c356d0ba4cdd183d29bb11", null ],
    [ "initial_time", "d1/db0/classLab0x04_1_1CollectionTask.html#ad4f24311b764eda384571ac3a8b2f48d", null ],
    [ "interval1", "d1/db0/classLab0x04_1_1CollectionTask.html#ae2a6c978495f2ea33d4dcdafbda34b76", null ],
    [ "interval2", "d1/db0/classLab0x04_1_1CollectionTask.html#a621cd3a5461dd8e38833a20429abed72", null ],
    [ "list", "d1/db0/classLab0x04_1_1CollectionTask.html#a8aa0e8b39394eba82443b0a23c282d8b", null ],
    [ "nxt_time", "d1/db0/classLab0x04_1_1CollectionTask.html#a7ceaae2e3d3e2d06ea85e1b04d7e6dfe", null ],
    [ "pin1", "d1/db0/classLab0x04_1_1CollectionTask.html#a3b9819ef3919b210622fcd95f1ac73d5", null ],
    [ "pin2", "d1/db0/classLab0x04_1_1CollectionTask.html#a4956bb2ba027559ca4b649a90ad04f8f", null ],
    [ "pos", "d1/db0/classLab0x04_1_1CollectionTask.html#a0b0c35a7fa9514eecc64c2abb7f6d7db", null ],
    [ "runs1", "d1/db0/classLab0x04_1_1CollectionTask.html#a44cbe5aad9cf9ad0f4380ca7e3c9598b", null ],
    [ "runs2", "d1/db0/classLab0x04_1_1CollectionTask.html#ac15a687c80c89da46389a1d6bf3ff62d", null ],
    [ "startp", "d1/db0/classLab0x04_1_1CollectionTask.html#a5f10b81f66753e66fad2c2dc85da76cf", null ],
    [ "startt", "d1/db0/classLab0x04_1_1CollectionTask.html#ab2ce39aec756782b66ae0ef31a564e57", null ],
    [ "state", "d1/db0/classLab0x04_1_1CollectionTask.html#a26b95d0ecd6da57e5fa5027def1e28a5", null ],
    [ "tim1", "d1/db0/classLab0x04_1_1CollectionTask.html#afba4cfc80c77e2ee985f444d262880e6", null ],
    [ "time", "d1/db0/classLab0x04_1_1CollectionTask.html#a7087f82ad00c28b8dbaaa93946213712", null ]
];