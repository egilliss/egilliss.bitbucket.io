var classLab0x07_1_1ClosedLoop =
[
    [ "__init__", "classLab0x07_1_1ClosedLoop.html#ae380291f5a96576cbba0725b8f3aba31", null ],
    [ "plotandformat", "classLab0x07_1_1ClosedLoop.html#a8f499ddaf278d03f36b439edb4bae033", null ],
    [ "run", "classLab0x07_1_1ClosedLoop.html#ada0c87b99003b0eb23952f8d13f225f8", null ],
    [ "savechar", "classLab0x07_1_1ClosedLoop.html#abad9aeb778dad76580edbffc05a0b408", null ],
    [ "sendchar", "classLab0x07_1_1ClosedLoop.html#abc29007dc9344a37c3eb09473fd818b7", null ],
    [ "transitionTo", "classLab0x07_1_1ClosedLoop.html#a2a8010fd71163a684428d69ddedfd970", null ],
    [ "gainFact", "classLab0x07_1_1ClosedLoop.html#aab39403bb9e1aaa18e29e5842e5e5308", null ],
    [ "initLoad", "classLab0x07_1_1ClosedLoop.html#afe46ced5f0ef7e8acff1b461c9342a65", null ],
    [ "interval", "classLab0x07_1_1ClosedLoop.html#a452d4c1627021174a581ff81de64dae7", null ],
    [ "refOmg", "classLab0x07_1_1ClosedLoop.html#ae95c490bf887e06930da112968ca528f", null ],
    [ "runs", "classLab0x07_1_1ClosedLoop.html#a6d74891fc78102bc6127e1d2448a5473", null ],
    [ "serial", "classLab0x07_1_1ClosedLoop.html#a0914783da14bac92e946378046d96d6a", null ],
    [ "state", "classLab0x07_1_1ClosedLoop.html#a73b11dd1f30c0034fb3037bf5abe4165", null ],
    [ "time_current", "classLab0x07_1_1ClosedLoop.html#a6e24403bde50fc25264c2b8537d1ded6", null ],
    [ "time_next", "classLab0x07_1_1ClosedLoop.html#a526a77c52f3d25403641d645bcc0d23c", null ],
    [ "time_start", "classLab0x07_1_1ClosedLoop.html#ab6b69cc8121a509d83b1c75781065329", null ],
    [ "timeToRun", "classLab0x07_1_1ClosedLoop.html#aa79ea97a94918475f8e5681c3e9c4efa", null ],
    [ "xvals", "classLab0x07_1_1ClosedLoop.html#ad819b56728ff4c9781e242fa9cff65f9", null ],
    [ "yvals", "classLab0x07_1_1ClosedLoop.html#a317cfc038cf122c2642590e9453e28cd", null ],
    [ "yvals2", "classLab0x07_1_1ClosedLoop.html#a8eb53540ca35d7bef9efaf67bece70ad", null ]
];