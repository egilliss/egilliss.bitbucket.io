var namespaces_dup =
[
    [ "Elevator", "namespaceElevator.html", null ],
    [ "Encoder", "namespaceEncoder.html", null ],
    [ "FibCalc", "namespaceFibCalc.html", null ],
    [ "Firmware", "namespaceFirmware.html", null ],
    [ "Lab0x02b", "namespaceLab0x02b.html", null ],
    [ "Lab0x04", "namespaceLab0x04.html", null ],
    [ "Lab0x05", "namespaceLab0x05.html", null ],
    [ "Lab0x06", "namespaceLab0x06.html", null ],
    [ "mainpage", "namespacemainpage.html", null ],
    [ "Physical", "namespacePhysical.html", null ],
    [ "UART0x05", "namespaceUART0x05.html", null ],
    [ "ui", "namespaceui.html", null ]
];