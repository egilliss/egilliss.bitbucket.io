var files_dup =
[
    [ "Elevator.py", "Elevator_8py.html", "Elevator_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "encoder", "classEncoder_1_1encoder.html", "classEncoder_1_1encoder" ]
    ] ],
    [ "FibCalc.py", "FibCalc_8py.html", "FibCalc_8py" ],
    [ "Firmware.py", "Firmware_8py.html", "Firmware_8py" ],
    [ "Lab0x02b.py", "Lab0x02b_8py.html", "Lab0x02b_8py" ],
    [ "Lab0x04.py", "Lab0x04_8py.html", "Lab0x04_8py" ],
    [ "Lab0x05.py", "Lab0x05_8py.html", "Lab0x05_8py" ],
    [ "Lab0x06.py", "Lab0x06_8py.html", "Lab0x06_8py" ],
    [ "Lab0x07.py", "Lab0x07_8py.html", "Lab0x07_8py" ],
    [ "Lab0x07Physical.py", "Lab0x07Physical_8py.html", [
      [ "Lab0x07Op", "classLab0x07Physical_1_1Lab0x07Op.html", "classLab0x07Physical_1_1Lab0x07Op" ]
    ] ],
    [ "Physical.py", "Physical_8py.html", [
      [ "Phys", "classPhysical_1_1Phys.html", "classPhysical_1_1Phys" ]
    ] ],
    [ "UART0x05.py", "UART0x05_8py.html", [
      [ "UART0x05", "classUART0x05_1_1UART0x05.html", "classUART0x05_1_1UART0x05" ]
    ] ],
    [ "ui.py", "ui_8py.html", "ui_8py" ]
];