var class_physical_1_1_phys =
[
    [ "__init__", "class_physical_1_1_phys.html#abd4e7265f17e1a98d908f95d1a301ae9", null ],
    [ "run", "class_physical_1_1_phys.html#a150ebe2fedd28d67f0a61f9cf747903f", null ],
    [ "Encoder", "class_physical_1_1_phys.html#a8a853db3d61109c6f2f35c7c8c179df9", null ],
    [ "input", "class_physical_1_1_phys.html#a662f0034722003eb68f5f39e6f036601", null ],
    [ "interval", "class_physical_1_1_phys.html#a8043efa0ea6ccb8f4f1e3d0d9f19d005", null ],
    [ "motor", "class_physical_1_1_phys.html#af179580b4c33ae115f89306e5a0ea064", null ],
    [ "myuart", "class_physical_1_1_phys.html#a3240f6ef35576b639e0084a2c29f0fb7", null ],
    [ "omega", "class_physical_1_1_phys.html#a9780ee8e840fc9dc06676ce92211a6b6", null ],
    [ "outpit", "class_physical_1_1_phys.html#a11de642233530a07e4fdddfb319873ff", null ],
    [ "runs", "class_physical_1_1_phys.html#a13bb7a1b86d4074e5355709f109b2c59", null ],
    [ "state", "class_physical_1_1_phys.html#a3bd2aaa40312fda8c81f990f1820ceb7", null ],
    [ "time_current", "class_physical_1_1_phys.html#ae9b4a9420ed2e0347af688c48a5536f0", null ],
    [ "time_next", "class_physical_1_1_phys.html#a0f77b1fd7057957d82595097b358bc03", null ],
    [ "time_start", "class_physical_1_1_phys.html#a9371298296d85734e11e414f3693b163", null ]
];