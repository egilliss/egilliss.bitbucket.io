var searchData=
[
  ['phys_55',['Phys',['../classPhysical_1_1Phys.html',1,'Physical']]],
  ['physical_2epy_56',['Physical.py',['../Physical_8py.html',1,'']]],
  ['pin_57',['pin',['../Elevator_8py.html#a51e360dac75f551ba72abbf5a23a698f',1,'Elevator']]],
  ['pin1_58',['Pin1',['../classEncoder_1_1encoder.html#a405920bcf16c8fb68b1fcafd50820f1b',1,'Encoder.encoder.Pin1()'],['../classLab0x04_1_1CollectionTask.html#a3b9819ef3919b210622fcd95f1ac73d5',1,'Lab0x04.CollectionTask.pin1()']]],
  ['pin2_59',['pin2',['../classLab0x04_1_1CollectionTask.html#a4956bb2ba027559ca4b649a90ad04f8f',1,'Lab0x04.CollectionTask.pin2()'],['../classEncoder_1_1encoder.html#ae8ef06f461d0bbecc33ee340ad8abd95',1,'Encoder.encoder.Pin2()']]],
  ['plotandformat_60',['plotandformat',['../classFirmware_1_1ClosedLoop.html#ab22abf1207a12b7b7c8cee68bbee0374',1,'Firmware.ClosedLoop.plotandformat()'],['../classLab0x07_1_1ClosedLoop.html#a8f499ddaf278d03f36b439edb4bae033',1,'Lab0x07.ClosedLoop.plotandformat()']]],
  ['port_61',['port',['../Firmware_8py.html#ae004223be4ddad7889f9407b317f3ad8',1,'Firmware.port()'],['../Lab0x07_8py.html#a31ba8459a8d6044e43d7435e658a20a3',1,'Lab0x07.port()']]],
  ['pos_62',['pos',['../classLab0x04_1_1CollectionTask.html#a0b0c35a7fa9514eecc64c2abb7f6d7db',1,'Lab0x04.CollectionTask.pos()'],['../Lab0x07_8py.html#a0a1ae17d2aa0b8794ee9351b8b86f9e5',1,'Lab0x07.pos()']]],
  ['position_63',['position',['../classEncoder_1_1encoder.html#ab9b40f45197fc4de2ba918de80d6823c',1,'Encoder::encoder']]]
];
