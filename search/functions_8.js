var searchData=
[
  ['savechar_168',['savechar',['../classFirmware_1_1ClosedLoop.html#a67f3a41c86999a01a39e479169d9f47e',1,'Firmware.ClosedLoop.savechar()'],['../classLab0x07_1_1ClosedLoop.html#abad9aeb778dad76580edbffc05a0b408',1,'Lab0x07.ClosedLoop.savechar()'],['../classLab0x07Physical_1_1Lab0x07Op.html#aa70d28f88e984b72f85dfc5f5175285a',1,'Lab0x07Physical.Lab0x07Op.savechar()'],['../classPhysical_1_1Phys.html#ad52fa65d7170c9c5d41a47f5a156e5e8',1,'Physical.Phys.savechar()']]],
  ['sendchar_169',['sendchar',['../classFirmware_1_1ClosedLoop.html#aedc923ad200fa9ec6e4880cb7fcf3cf1',1,'Firmware.ClosedLoop.sendchar()'],['../classLab0x07_1_1ClosedLoop.html#abc29007dc9344a37c3eb09473fd818b7',1,'Lab0x07.ClosedLoop.sendchar()'],['../classLab0x07Physical_1_1Lab0x07Op.html#a58cad387accc0b160b95f900b070d4b6',1,'Lab0x07Physical.Lab0x07Op.sendchar()'],['../classPhysical_1_1Phys.html#ac67d797690d6723af9aeedbd39965461',1,'Physical.Phys.sendchar()']]],
  ['set_5fduty_170',['set_duty',['../classLab0x06_1_1MotorDriver.html#a11ddc1bd9a171532f1a962d15cd74f29',1,'Lab0x06::MotorDriver']]],
  ['stop_171',['Stop',['../Elevator_8py.html#ae1c3b3ed76354188d0848bfd8130e032',1,'Elevator']]]
];
