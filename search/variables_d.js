var searchData=
[
  ['read_213',['read',['../Lab0x04_8py.html#a6a6a4cda2028eaf377ee883a82ff6743',1,'Lab0x04']]],
  ['refer_214',['refer',['../Lab0x07_8py.html#ab2d703e8f04540910b74c520daa7c322',1,'Lab0x07']]],
  ['refomg_215',['refOmg',['../classFirmware_1_1ClosedLoop.html#af99513a98e3bd1ddbf194029f695cb8b',1,'Firmware.ClosedLoop.refOmg()'],['../classLab0x07_1_1ClosedLoop.html#ae95c490bf887e06930da112968ca528f',1,'Lab0x07.ClosedLoop.refOmg()'],['../Firmware_8py.html#a5ad95f4d6aa3065c34f4318604f2f6f7',1,'Firmware.refOmg()']]],
  ['runs_216',['runs',['../classFirmware_1_1ClosedLoop.html#a91de2d3c50ea10ccaba1e347311e638f',1,'Firmware.ClosedLoop.runs()'],['../classLab0x02b_1_1vLED.html#a6f1ceda46b54839b01a5e4d4e4a8acaf',1,'Lab0x02b.vLED.runs()'],['../classLab0x05_1_1UIBlinker.html#a6f148ca057ca1eb0270f3139c6958099',1,'Lab0x05.UIBlinker.runs()'],['../classLab0x07_1_1ClosedLoop.html#a6d74891fc78102bc6127e1d2448a5473',1,'Lab0x07.ClosedLoop.runs()'],['../classLab0x07Physical_1_1Lab0x07Op.html#a0b76b48cd9216ce08f724fef090ea2b9',1,'Lab0x07Physical.Lab0x07Op.runs()'],['../classPhysical_1_1Phys.html#a13bb7a1b86d4074e5355709f109b2c59',1,'Physical.Phys.runs()'],['../Elevator_8py.html#ae8b2650ca4a022852e691c182311f912',1,'Elevator.runs()'],['../Lab0x02b_8py.html#a09b442b1b9ff7cf00e2dd2a0a710fda8',1,'Lab0x02b.runs()']]],
  ['runs1_217',['runs1',['../classLab0x04_1_1CollectionTask.html#a44cbe5aad9cf9ad0f4380ca7e3c9598b',1,'Lab0x04::CollectionTask']]],
  ['runs2_218',['runs2',['../classLab0x04_1_1CollectionTask.html#ac15a687c80c89da46389a1d6bf3ff62d',1,'Lab0x04::CollectionTask']]]
];
