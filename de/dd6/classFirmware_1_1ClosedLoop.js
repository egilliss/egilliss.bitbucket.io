var classFirmware_1_1ClosedLoop =
[
    [ "__init__", "de/dd6/classFirmware_1_1ClosedLoop.html#a1f7e58ec243165e837014ca23da6401c", null ],
    [ "plotandformat", "de/dd6/classFirmware_1_1ClosedLoop.html#ab22abf1207a12b7b7c8cee68bbee0374", null ],
    [ "run", "de/dd6/classFirmware_1_1ClosedLoop.html#a7142c09bb91adca9432369b7eb69fd51", null ],
    [ "savechar", "de/dd6/classFirmware_1_1ClosedLoop.html#a67f3a41c86999a01a39e479169d9f47e", null ],
    [ "sendchar", "de/dd6/classFirmware_1_1ClosedLoop.html#aedc923ad200fa9ec6e4880cb7fcf3cf1", null ],
    [ "transitionTo", "de/dd6/classFirmware_1_1ClosedLoop.html#a3d0cde78f921fbd0c6e72f467be28faf", null ],
    [ "gainFact", "de/dd6/classFirmware_1_1ClosedLoop.html#a38a4d3eb57b63ce5eb87e0d58eb86d7c", null ],
    [ "initLoad", "de/dd6/classFirmware_1_1ClosedLoop.html#aff4db224ef88d4d394e934afe9004d39", null ],
    [ "interval", "de/dd6/classFirmware_1_1ClosedLoop.html#a376f209e0265d36e9403aac87018a2ef", null ],
    [ "refOmg", "de/dd6/classFirmware_1_1ClosedLoop.html#af99513a98e3bd1ddbf194029f695cb8b", null ],
    [ "runs", "de/dd6/classFirmware_1_1ClosedLoop.html#a91de2d3c50ea10ccaba1e347311e638f", null ],
    [ "serial", "de/dd6/classFirmware_1_1ClosedLoop.html#a69d544da97da57345b37b0a9c742c029", null ],
    [ "state", "de/dd6/classFirmware_1_1ClosedLoop.html#ada5f860f67d84a798f14feac1ea047dd", null ],
    [ "time_current", "de/dd6/classFirmware_1_1ClosedLoop.html#a71ff8e6e1ae10320fa4eb3e8a2b349f6", null ],
    [ "time_next", "de/dd6/classFirmware_1_1ClosedLoop.html#a71529b224448662621a195d58e12b102", null ],
    [ "time_start", "de/dd6/classFirmware_1_1ClosedLoop.html#ab792c6af6ce9fcbc549a82c7a6c4ca19", null ],
    [ "timeToRun", "de/dd6/classFirmware_1_1ClosedLoop.html#a0cb8f05a1969110823ea71a7596ca746", null ],
    [ "xvals", "de/dd6/classFirmware_1_1ClosedLoop.html#a7586319b1a0fb39eb5bdd56b7e0d03b3", null ],
    [ "yvals", "de/dd6/classFirmware_1_1ClosedLoop.html#a4e9a3a2a21283f601a2d41f30d20d237", null ],
    [ "yvals2", "de/dd6/classFirmware_1_1ClosedLoop.html#a5fa7f048e6fb504e13a011a4a5539a53", null ]
];