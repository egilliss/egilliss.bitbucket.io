var classPhysical_1_1Phys =
[
    [ "__init__", "d5/df7/classPhysical_1_1Phys.html#abd4e7265f17e1a98d908f95d1a301ae9", null ],
    [ "run", "d5/df7/classPhysical_1_1Phys.html#a150ebe2fedd28d67f0a61f9cf747903f", null ],
    [ "Encoder", "d5/df7/classPhysical_1_1Phys.html#a8a853db3d61109c6f2f35c7c8c179df9", null ],
    [ "input", "d5/df7/classPhysical_1_1Phys.html#a662f0034722003eb68f5f39e6f036601", null ],
    [ "interval", "d5/df7/classPhysical_1_1Phys.html#a8043efa0ea6ccb8f4f1e3d0d9f19d005", null ],
    [ "motor", "d5/df7/classPhysical_1_1Phys.html#af179580b4c33ae115f89306e5a0ea064", null ],
    [ "myuart", "d5/df7/classPhysical_1_1Phys.html#a3240f6ef35576b639e0084a2c29f0fb7", null ],
    [ "omega", "d5/df7/classPhysical_1_1Phys.html#a9780ee8e840fc9dc06676ce92211a6b6", null ],
    [ "outpit", "d5/df7/classPhysical_1_1Phys.html#a11de642233530a07e4fdddfb319873ff", null ],
    [ "runs", "d5/df7/classPhysical_1_1Phys.html#a13bb7a1b86d4074e5355709f109b2c59", null ],
    [ "state", "d5/df7/classPhysical_1_1Phys.html#a3bd2aaa40312fda8c81f990f1820ceb7", null ],
    [ "time_current", "d5/df7/classPhysical_1_1Phys.html#ae9b4a9420ed2e0347af688c48a5536f0", null ],
    [ "time_next", "d5/df7/classPhysical_1_1Phys.html#a0f77b1fd7057957d82595097b358bc03", null ],
    [ "time_start", "d5/df7/classPhysical_1_1Phys.html#a9371298296d85734e11e414f3693b163", null ]
];